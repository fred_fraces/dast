# GitLab DAST changelog

## v1.14.0
- The DAST JSON report is created using information from the ZAP REST API, not the ZAP JSON report (!142)
- Set the maximum duration of the spider scan with environment variable `DAST_SPIDER_MINS` (!153)
- Include alpha passive and active scan rules with environment variable `DAST_INCLUDE_ALPHA_VULNERABILITIES` (!153)
- Set ZAP config URL to INFO, IGNORE or FAIL warnings with environment variable `DAST_ZAP_CONFIG_URL` (!153)

## v1.13.3
- Enable Ajax spider using the environment variable `DAST_ZAP_USE_AJAX_SPIDER` (!147)

## v1.13.2
- ZAP does not make external HTTP requests when `auto-update-addons` is false (!146)
- Upgrade ZAP add-on `fuzzdb` to [v6](https://github.com/zaproxy/zap-extensions/releases/tag/fuzzdb-v6) (!144)
- Upgrade ZAP add-on `pscanrules-release` to [v28](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v28) (!144)
- Upgrade ZAP add-on `selenium-release` to [v15.2.0](https://github.com/zaproxy/zap-extensions/releases/tag/selenium-v15.2.0) (!144)

## v1.13.1
- Disable auto-updating ZAP addons by default (!143)

## v1.13.0
- Each Vulnerability is now identifiable by a unique id in the JSON report (!128)

## v1.12.1
- Rename `CommonReportFormatter` to `SecurityReportFormatter` (!138)

## v1.12.0
- `vulnerabilities[].evidence.summary` is added to the report to provide more context about the vulnerability (!132)

## v1.11.1
- `severity` and `confidence` report values are title case to match the DAST schema (!131)

## v1.11.0
- OpenAPI specifications can be referenced using the `DAST_API_SPECIFICATION` environment variable to trigger an API scan (!92)(!112)(!125)(!126)
- Hosts defined in API specifications can be overridden using the `DAST_API_HOST_OVERRIDE` environment variable (!112)(!123)
- Validation rules can be excluded from the scan using the `DAST_EXCLUDE_RULES` environment variable (!115)
- Request headers can be added to every request using the `DAST_REQUEST_HEADERS` environment variable (!124)

## v1.10.0
- Limit URLs displayed in console output to those captured during spider scan (!113)
- Output URLs spidered by DAST JSON report as `scan.scanned_resources` (!113)
- Logging is `INFO` level by default (!110)

## v1.9.0
- Include URLs scanned by DAST in the console output (!112)

## v1.8.0
- DAST depends on ZAP weekly release w2020-02-10 (!101)
- DAST Python scripts run using Python 3.6.9 in anticipation of Python 2 EOL (!102)
- Upgrade pinned add-on versions: alertFilters to [v10](https://github.com/zaproxy/zap-extensions/releases/tag/alertFilters-v10), ascanrulesBeta to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/ascanrulesBeta-v27) and pscanrules to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v27) (!107)

## v1.7.0
- Include recent ZAP add-ons into the DAST Docker image so they don't have to be downloaded every scan (!105)
- Auto-update of ZAP add-ons can be disabled with the `--auto-update-addons false` command line argument (!105)

## v1.6.1
- Fix issue where AJAX spider scans could not start Firefox (!87)

## v1.6.0
- Add initial Secure Common Report Format fields to DAST report (!81)

## v1.5.4
- Validate URL command line arguments (!69)
- Exit code is zero when a scan runs successfully, non zero when a scan fails or arguments are invalid (!69)
- The DAST report is deterministic, keys in the JSON document are in alphabetical order (!70)
- Ajax scans start from the target URL (!71)
- Don't verify HTTPS certificates when determining if a target site is ready for scanning (!76)

## v1.5.3
- Fixed support for `--auth-exclude-urls` parameter (!52)

## v1.5.2
- DAST depends on ZAP weekly release w2019-09-24, re-enabling `ascanrulesBeta` rules
- Removed Python 3 to fix Full Scans

## v1.5.1
- DAST depends on ZAP release 2.8.0 (!50)

## v1.5.0
- Running `/analyze --help` shows all options and environment variables supported by DAST (!39)
- Expose ZAP logs while executing scans (!42)

## v1.4.0
- Implement [domain validation](https://docs.gitlab.com/ee/user/application_security/dast/index.html#domain-validation) option for full scans (!35)

## v1.3.0
- Report which URLs were scanned (!24)

## v1.2.7
- Fix passing of optional params to ZAP (!27)

## v1.2.6
- Fix max. curl timeout to be longer than 150 seconds (!26)

## v1.2.5
- Fix curl timeout (!25)

## v1.2.4
- Fix timeout when $DAST_TARGET_AVAILABILITY_TIMEOUT is used (!21)

## v1.2.3
- Fix auto login functionality. Auto login is used if the HTML elements for username, password, or submit button have not been specified.

## v1.2.2
- Fix a bug where `analyze` would fail if only `DAST_WEBSITE` was used. https://gitlab.com/gitlab-org/gitlab-ee/issues/11744

## v1.2.1
- Accept $DAST_WEBSITE env var instead of `-t` parameter (still supported for backward compatibility)

## v1.2.0
- Add [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) support (!14)

## v1.1.2
- Add workaround for supporting long CLI auth options (!9)

## v1.1.1
- Fix a problem with multiple login buttons on the login page.

## v1.1.0
- First release of the DAST GitLab image.
