

class ScannedResources:

    def __init__(self, messages):
        self.__urls = []
        self.__scanned_resources = []
        self.__messages = messages

    def __format(self):
        self.__urls = []
        self.__scanned_resources = []

        for msg in self.__messages:
            url_string = f"{msg['method']} {msg['url']}"

            if url_string not in self.__urls:
                self.__scanned_resources.append({'method': msg['method'], 'url': msg['url']})
                self.__urls.append(url_string)

    def __getitem__(self, message_index):
        return self.__messages[message_index]

    def __len__(self):
        return len(self.__messages)

    def count(self):
        self.__format()
        return len(self.__urls)

    def scanned_resources_as_string(self):
        self.__format()
        return '\n'.join(self.__urls)

    def scanned_resources(self):
        self.__format()
        return self.__scanned_resources
