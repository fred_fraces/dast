from collections import OrderedDict
from .url import URL
import itertools


class ModifiedZapReportFormatter:

    def format(self, zap_report, alerts, scanned_resources, spider_scan, spider_scan_results):
        formatted = OrderedDict()

        if zap_report is None or len(zap_report) == 0:
            raise RuntimeError('Unable to create DAST report as there is no ZAP report')

        if spider_scan is None or len(spider_scan) == 0:
            raise RuntimeError('Unable to create DAST report as there is no spider scan')

        if spider_scan_results is None or len(spider_scan_results) == 0:
            raise RuntimeError('Unable to create DAST report as there are no spider scan results')

        # Legacy DAST format
        formatted['@generated'] = zap_report.get('@generated', '')
        formatted['@version'] = zap_report.get('@version', '')
        formatted['site'] = self.__format_sites(alerts)
        formatted['spider'] = spider = OrderedDict()
        spider['progress'] = spider_scan.get('progress', '')
        spider['result'] = OrderedDict()
        spider['result']['urlsInScope'] = self.__format_urls(self.__collect(spider_scan_results, 'urlsInScope'))
        spider['result']['urlsIoError'] = self.__format_urls(self.__collect(spider_scan_results, 'urlsIoError'))
        spider['result']['urlsOutOfScope'] = sorted(self.__collect(spider_scan_results, 'urlsOutOfScope'))
        spider['state'] = spider_scan.get('state', '')

        return formatted

    def __collect(self, values, property):
        properties = [value.get(property, []) for value in values]
        return list(itertools.chain(*properties))

    def __format_sites(self, alerts):
        formatted = []

        sorted_alerts = sorted(alerts, key=lambda a: self.__group_sites_by(a))

        for _, host_alerts in itertools.groupby(sorted_alerts, lambda a: self.__group_sites_by(a)):
            formatted.append(self.__format_site(list(host_alerts)))

        return formatted

    def __group_sites_by(self, alert):
        url = URL.parse(alert.get('url', ''))
        return url.scheme_and_host

    def __format_site(self, alerts):
        if len(alerts) == 0:
            raise RuntimeError('Attempting to format site when there are no alerts')

        url = URL.parse(alerts[0].get('url', ''))

        formatted = OrderedDict()
        formatted['@host'] = url.host
        formatted['@name'] = url.scheme_and_host
        formatted['@port'] = str(url.port)
        formatted['@ssl'] = 'true' if url.is_secure else 'false'
        formatted['alerts'] = self.__format_alerts(alerts)
        return formatted

    def __format_alerts(self, alerts):
        formatted = []
        sorted_alerts = sorted(alerts, key=lambda a: self.__group_alerts_by(a))

        for _, plugin_alerts in itertools.groupby(sorted_alerts, lambda a: self.__group_alerts_by(a)):
            formatted.append(self.__format_alert(list(plugin_alerts)))

        return sorted(formatted, key=lambda a: (-int(a.get('riskcode', '1')), a.get('name', '')))

    def __group_alerts_by(self, alert):
        return f"{alert.get('pluginId', '')}::{alert.get('risk', '')}::{alert.get('confidence', '')}"

    def __format_alert(self, alerts):
        if len(alerts) == 0:
            raise RuntimeError('Unable to format alerts when there are none present')

        first_alert = alerts[0]

        formatted = OrderedDict()
        formatted['alert'] = first_alert.get('alert', '')
        formatted['confidence'] = self.__format_confidence(first_alert.get('confidence', ''))
        formatted['count'] = str(len(alerts))
        formatted['cweid'] = self.__format_non_zero_text_digit(first_alert.get('cweid', ''))
        formatted['desc'] = self.__format_lines_as_html(first_alert.get('description', ''))
        formatted['instances'] = self.__format_alert_instances(alerts)
        formatted['name'] = first_alert.get('name', '')
        formatted['otherinfo'] = self.__format_lines_as_html(first_alert.get('other', ''))
        formatted['pluginid'] = first_alert.get('pluginId', '')
        formatted['reference'] = self.__format_lines_as_html(first_alert.get('reference', ''), empty_value='<p></p>')
        formatted['riskcode'] = self.__format_riskcode(first_alert.get('risk', ''))
        formatted['riskdesc'] = f"{first_alert.get('risk', '')} ({first_alert.get('confidence', '')})"
        formatted['solution'] = self.__format_lines_as_html(first_alert.get('solution', ''), empty_value='<p></p>')
        formatted['sourceid'] = first_alert.get('sourceid', '')
        formatted['wascid'] = self.__format_non_zero_text_digit(first_alert.get('wascid', ''))

        if not formatted['pluginid']:
            raise RuntimeError("Unable to create DAST report as "
                               "there is no pluginid for alert '{}'".format(first_alert.get('id', '')))

        return formatted

    def __format_non_zero_text_digit(self, value):
        if not value or not value.isdigit() or value == '0':
            return ''

        return value

    def __format_lines_as_html(self, content, empty_value=''):
        if not content:
            return empty_value

        paragraphs = [f'<p>{line}</p>' for line in content.replace('\r', '').split("\n")]
        return ''.join(paragraphs)

    def __format_riskcode(self, zap_risk):
        return {'Informational': '0', 'Low': '1', 'Medium': '2', 'High': '3'}.get(zap_risk, '0')

    def __format_confidence(self, zap_confidence):
        return {'False Positive': '0',
                'Low': '1',
                'Medium': '2',
                'High': '3',
                'Confirmed': '4'}.get(zap_confidence, '1')

    def __format_alert_instances(self, alerts):
        sorted_alerts = sorted(alerts, key=self.sort_by('url', 'method', 'param', 'attack'))
        return [self.__format_alert_instance(alert) for alert in sorted_alerts]

    def __format_alert_instance(self, alert):
        formatted = OrderedDict()
        formatted['attack'] = alert.get('attack', '')
        formatted['evidence'] = alert.get('evidence', '')
        formatted['method'] = alert.get('method', '')
        formatted['param'] = alert.get('param', '')
        formatted['uri'] = alert.get('url', '')
        return formatted

    def __format_urls(self, urls):
        sorted_urls = sorted(urls, key=self.sort_by('url', 'method'))
        return [self.__format_url(url) for url in sorted_urls]

    def __format_url(self, url):
        formatted = OrderedDict()
        formatted['method'] = url.get('method', '')
        formatted['processed'] = url.get('processed', '')
        formatted['reasonNotProcessed'] = url.get('reasonNotProcessed', '')
        formatted['statusCode'] = url.get('statusCode', '')
        formatted['statusReason'] = url.get('statusReason', '')
        formatted['url'] = url.get('url', '')

        return formatted

    def sort_by(self, *properties):
        return lambda value: ':'.join([value.get(prop, '') for prop in properties])
