from collections import OrderedDict
from .url import URL
import re


class SecurityReportFormatter:

    def __init__(self, wasc, uuid):
        self.wasc = wasc
        self.uuid = uuid

    def format(self, zap_report, alerts, scanned_resources, spider_scan='', spider_scan_results=''):
        formatted = OrderedDict()

        if zap_report is None or len(zap_report) == 0:
            raise RuntimeError('Unable to create Common Format Report as there is no ZAP report')

        formatted['remediations'] = []
        formatted['version'] = '2.3'
        formatted['vulnerabilities'] = self.__format_vulnerabilities(alerts)
        formatted['scan'] = {
            'scanned_resources': self.__format_scanned_resources(scanned_resources)
        }

        return formatted

    def __format_scanned_resources(self, scanned_resources):
        resources = scanned_resources.scanned_resources()
        scanned_resources = [self.__format_scanned_resource(resource) for resource in resources]

        sorted_scanned_resources = sorted(scanned_resources, key=lambda resource: (resource['url'], resource['method']))
        return sorted_scanned_resources

    def __format_scanned_resource(self, scanned_resource):
        formatted = OrderedDict()
        formatted['method'] = scanned_resource['method']
        formatted['type'] = 'url'
        formatted['url'] = scanned_resource['url']

        return formatted

    def __format_vulnerabilities(self, alerts):
        sorted_alerts = sorted(alerts, key=lambda a: (
            -int(self.__format_riskcode(a.get('risk', 'Informational'))),
            a.get('name', ''),
            a.get('method', ''),
            a.get('url', ''),
            a.get('param', '')))

        return [self.__format_vulnerability(alert) for alert in sorted_alerts]

    def __format_vulnerability(self, alert):
        formatted = OrderedDict()
        formatted['category'] = 'dast'
        formatted['confidence'] = self.__format_confidence(alert.get('confidence', ''))
        formatted['cve'] = alert.get('pluginId', '')
        formatted['description'] = self.__remove_new_lines(alert.get('description', ''))
        formatted['evidence'] = self.__format_evidence(alert.get('evidence', ''), alert.get('other', ''))
        formatted['id'] = str(self.uuid.uuid4())
        formatted['identifiers'] = (self.__format_plugin_identifier(alert.get('name', ''), alert.get('pluginId', '')) +
                                    self.__format_cwe_identifier(alert.get('cweid', '')) +
                                    self.__format_wasc_identifier(alert.get('wascid', '')))
        formatted['links'] = self.__format_links(alert.get('reference', ''))
        formatted['location'] = self.__format_location(alert.get('url', ''),
                                                       alert.get('method', ''),
                                                       alert.get('param', ''))
        formatted['message'] = alert.get('name', '')
        formatted['scanner'] = {'id': 'zaproxy', 'name': 'ZAProxy'}
        formatted['severity'] = self.__format_severity(alert.get('risk', ''))
        formatted['solution'] = self.__remove_new_lines(alert.get('solution', ''))

        if not formatted['cve']:
            raise RuntimeError("Unable to create DAST report as "
                               f"there is no pluginId for alert '{formatted['message']}'")

        return formatted

    def __format_evidence(self, evidence, otherinfo):
        formatted = OrderedDict()
        formatted['summary'] = self.__format_evidence_summary(evidence, otherinfo)
        return formatted

    def __format_evidence_summary(self, evidence, otherinfo):
        values = [evidence.strip(),
                  self.__remove_new_lines(otherinfo)]

        return '; '.join([content for content in values if len(content) > 0])

    def __format_riskcode(self, zap_risk):
        return {'Informational': '0', 'Low': '1', 'Medium': '2', 'High': '3'}.get(zap_risk, '1')

    def __format_severity(self, zap_risk):
        return {'Informational': 'Info', 'Low': 'Low', 'Medium': 'Medium', 'High': 'High'}.get(zap_risk, 'Unknown')

    def __format_confidence(self, zap_confidence):
        return {'False Positive': 'Ignore',
                'Low': 'Low',
                'Medium': 'Medium',
                'High': 'High',
                'Confirmed': 'Confirmed'}.get(zap_confidence, 'Unknown')

    def __format_links(self, reference):
        return [{'url': url} for url in reference.split('\n') if url]

    def __format_location(self, uri, method, param):
        url = URL.parse(uri)

        formatted = OrderedDict()
        formatted['hostname'] = url.scheme_and_host
        formatted['method'] = method
        formatted['param'] = param
        formatted['path'] = url.path_and_query_fragment
        return formatted

    def __format_plugin_identifier(self, vulnerability_name, plugin_id):
        formatted = OrderedDict()
        formatted['name'] = vulnerability_name
        formatted['type'] = 'ZAProxy_PluginId'
        formatted['url'] = 'https://github.com/zaproxy/zaproxy/blob/w2019-01-14/docs/scanners.md'
        formatted['value'] = plugin_id
        return [formatted]

    def __format_cwe_identifier(self, cwe_id):
        if not cwe_id or not cwe_id.isdigit() or cwe_id == '0':
            return []

        formatted = OrderedDict()
        formatted['name'] = f'CWE-{cwe_id}'
        formatted['type'] = 'CWE'
        formatted['url'] = f'https://cwe.mitre.org/data/definitions/{cwe_id}.html'
        formatted['value'] = cwe_id
        return [formatted]

    def __format_wasc_identifier(self, wasc_id):
        if not wasc_id or not wasc_id.isdigit() or wasc_id == '0':
            return []

        formatted = OrderedDict()
        formatted['name'] = f'WASC-{wasc_id}'
        formatted['type'] = 'WASC'
        formatted['url'] = self.wasc.reference_url_for_id(wasc_id)
        formatted['value'] = wasc_id
        return [formatted]

    def __remove_new_lines(self, content):
        single_line = (content or '').replace('\n', ' ').strip()
        return re.sub(r'\s+', ' ', single_line)
