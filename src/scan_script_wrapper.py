from itertools import chain


class ScanScriptWrapper():

    def __init__(self, original_main, config, log, target_website, system):
        self.original_main = original_main
        self.config = config
        self.log = log
        self.target_website = target_website
        self.system = system

    def run(self):
        if not self.__verify_target_website():
            return self.system.exit(1)

        # Hide "Starting new HTTP connection" messages
        self.log.getLogger("requests").setLevel(self.log.DEBUG)

        self.log.info("starting scan")
        self.original_main.main(self.__zap_arguments())

    def __verify_target_website(self):
        if self.config.api_specification:
            return True

        if not self.target_website.is_configured():
            self.log.error('Target website is not configured.')
            self.log.error('See https://docs.gitlab.com/ee/user/application_security/dast/ for instructions '
                           'on how to configure DAST, or run `/analyze --help`.')
            return False

        self.log.info(f'using Python {self.system.version()}')

        if self.target_website.is_url():
            is_safe_to_scan, safety_error = self.__probe_target_website()

            if not is_safe_to_scan:
                domain_validation_url = 'https://docs.gitlab.com/ee/user/application_security/dast/#domain-validation'
                self.log.error(f'domain validation failed due to: {safety_error}, see {domain_validation_url}')
                return False

        return True

    def __probe_target_website(self):
        self.log.info(f'waiting for {self.target_website.address()} to be available')

        site_check = self.target_website.check_site_is_available()

        if not site_check.is_available():
            self.log.warn(f"{self.config.target} could not be reached, attempting scan anyway")
            self.log.debug(f"last attempted access of target caused error {site_check.unavailable_reason()}")

        return site_check.is_safe_to_scan()

    def __zap_arguments(self):
        args = [
            self.__get_target(),
            ['-J', 'gl-dast-report.json'],
            self.__get_argument('zap_config_file', '-c'),
            self.__get_argument('zap_config_url', '-u'),
            self.__get_argument('zap_gen_file', '-g'),
            self.__get_argument('spider_mins', '-m'),
            self.__get_argument('zap_report_html', '-r'),
            self.__get_argument('zap_report_md', '-w'),
            self.__get_argument('zap_report_xml', '-x'),
            self.__get_argument('zap_include_alpha', '-a', boolean_flag=True),
            self.__get_argument('zap_debug', '-d', boolean_flag=True),
            self.__get_argument('zap_port', '-P'),
            self.__get_argument('zap_delay_in_seconds', '-D'),
            self.__get_argument('zap_default_info', '-i', boolean_flag=True),
            self.__get_argument('zap_no_fail_on_warn', '-I', boolean_flag=True),
            self.__get_argument('zap_use_ajax_spider', '-j', boolean_flag=True),
            self.__get_argument('zap_min_level', '-l'),
            self.__get_argument('zap_context_file', '-n'),
            self.__get_argument('zap_progress_file', '-p'),
            self.__get_argument('zap_short_format', '-s', boolean_flag=True),
            self.__get_argument('zap_mins_to_wait', '-T'),
            self.__get_argument('zap_api_host_override', '-O'),
            self.__get_zap_api_scan_type(),
            self.__get_zap_api_scan_format(),
            self.__get_zap_other_options()]

        return list(chain.from_iterable(args))

    def __get_target(self):
        if self.config.api_specification:
            return ['-t', self.config.api_specification]

        return ['-t', self.config.target]

    def __get_zap_api_scan_type(self):
        if self.config.api_specification and not self.config.full_scan:
            return ['-S']

        return []

    def __get_zap_api_scan_format(self):
        if self.config.api_specification:
            return ['-f', 'openapi']

        return []

    def __get_zap_other_options(self):
        options = [self.config.zap_other_options] if self.config.zap_other_options else []

        options.extend(self.__get_exclude_rules())
        options.extend(self.__get_custom_request_headers())
        options.extend(self.__get_dast_zap_configuration())

        return ['-z', ' '.join(options)]

    def __get_dast_zap_configuration(self):
        opts = ['-config selenium.firefoxDriver=/usr/bin/geckodriver']
        opts.append('-addonupdate') if self.config.auto_update_addons else ''
        opts.append('-silent') if self.config.silent else ''
        return opts

    def __get_exclude_rules(self):
        rules = []

        for index, rule_id in enumerate(self.config.exclude_rules or []):
            rules.extend([
                f'-config globalalertfilter.filters.filter({index}).ruleid={rule_id}',
                f'-config globalalertfilter.filters.filter({index}).url=.*',
                f'-config globalalertfilter.filters.filter({index}).urlregex=true',
                f'-config globalalertfilter.filters.filter({index}).newrisk=-1',
                f'-config globalalertfilter.filters.filter({index}).enabled=true'
            ])

        return rules

    def __get_custom_request_headers(self):
        rules = []

        for index, (name, value) in enumerate((self.config.request_headers or {}).items()):
            rules.extend([
                f'-config replacer.full_list({index}).description=header_{index}',
                f'-config replacer.full_list({index}).enabled=true',
                f'-config replacer.full_list({index}).matchtype=REQ_HEADER',
                f'-config replacer.full_list({index}).matchstr={name}',
                f'-config replacer.full_list({index}).regex=false',
                f'-config replacer.full_list({index}).replacement="{value}"'
            ])

        return rules

    def __get_argument(self, name, option_name, boolean_flag=False):
        option = getattr(self.config, name, None)

        if option:
            if boolean_flag:
                return [option_name]
            return [option_name, getattr(self.config, name)]
        return []
