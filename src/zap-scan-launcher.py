#!/usr/bin/env python

from dependencies import config, FullScan, BaselineScan, APIScan

if __name__ == "__main__":
    if config.is_api_scan:
        APIScan.run()
    elif config.full_scan:
        FullScan.run()
    else:
        BaselineScan.run()
