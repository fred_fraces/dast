import sys
import pprint


class System:

    def __init__(self):
        self.pretty_printer = pprint.PrettyPrinter(indent=4)

    def notify(self, message):
        print(message)

    def pretty_notify(self, obj):
        self.pretty_printer.pprint(obj)

    def version(self):
        return " ".join([x.strip() for x in sys.version.splitlines()])

    def exit(self, exit_code):
        sys.exit(exit_code)
