from argparse import Action, ArgumentParser, RawDescriptionHelpFormatter, ArgumentTypeError
from six.moves.urllib.parse import urlparse


class FallbackToEnvironment(Action):
    def __init__(self, environment_variables, environment, **kwargs):
        default = kwargs.get('default', None)

        if environment and environment_variables:
            for var in environment_variables:
                default = environment[var] if var in environment else default

        kwargs.pop('default', None)
        super(FallbackToEnvironment, self).__init__(default=default, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        attr_values = True if self.nargs is 0 else values
        setattr(namespace, self.dest, attr_values)


class Configuration:

    @classmethod
    def load(cls, argv, environment):
        description = """
Run GitLab DAST against a target website of your choice.

Arguments can be supplied via command line and will fallback to using the outlined environment variable.
"""

        parser = ArgumentParser(description=description,
                                formatter_class=RawDescriptionHelpFormatter)

        # DAST configuration options
        parser.add_argument('-t', dest='target', action=FallbackToEnvironment, environment=environment,
                            environment_variables=['DAST_WEBSITE'],
                            help='The URL of the website to scan (env DAST_WEBSITE)')
        parser.add_argument('--api-specification', dest='api_specification', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_API_SPECIFICATION'],
                            help='The URL of the website to scan (env DAST_WEBSITE)')
        parser.add_argument('--auth-url', dest='auth_url', type=cls.__url, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_URL', 'AUTH_URL'],
                            help='login form URL (env DAST_AUTH_URL)')
        parser.add_argument('--auth-username', dest='auth_username', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME', 'AUTH_USERNAME'],
                            help='login form username (env DAST_USERNAME)')
        parser.add_argument('--auth-password', dest='auth_password', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD', 'AUTH_PASSWORD'],
                            help='login form password (env DAST_PASSWORD)')
        parser.add_argument('--auth-username-field', dest='auth_username_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME_FIELD', 'AUTH_USERNAME_FIELD'],
                            help='login form name of username input field (env DAST_USERNAME_FIELD)')
        parser.add_argument('--auth-password-field', dest='auth_password_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD_FIELD', 'AUTH_PASSWORD_FIELD'],
                            help='login form name of password input field (env DAST_PASSWORD_FIELD)')
        parser.add_argument('--auth-submit-field', dest='auth_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SUBMIT_FIELD', 'AUTH_SUBMIT_FIELD'],
                            help='login form name or value of submit input (env AUTH_SUBMIT_FIELD)')
        parser.add_argument('--auth-first-submit-field', dest='auth_first_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FIRST_SUBMIT_FIELD', 'AUTH_FIRST_SUBMIT_FIELD'],
                            help='login form name or value of submit input of first page (env AUTH_FIRST_SUBMIT_FIELD)')
        parser.add_argument('--auth-display', dest='auth_display', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_DISPLAY', 'AUTH_DISPLAY'],
                            help='set the virtual display to be visible (env AUTH_DISPLAY)')
        parser.add_argument('--auth-auto', dest='auth_auto', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_AUTO', 'AUTH_AUTO'],
                            help='login for automatically finds login fields (env AUTH_AUTO)')
        parser.add_argument('--auth-exclude-urls', dest='auth_exclude_urls', type=cls.__list_of_urls,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_EXCLUDE_URLS', 'AUTH_EXCLUDE_URLS'],
                            help='comma separated list of URLs to exclude, no spaces in between, supply all ' +
                                 'URLs causing logout (env AUTH_EXCLUDE_URLS)')
        parser.add_argument('--request-headers', dest='request_headers', type=cls.__dict_of_key_values,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_REQUEST_HEADER', 'DAST_REQUEST_HEADERS'],
                            help='comma separated list of name: value request headers, '
                                 'these will be added to every request made by ZAProxy e.g. "Cache-control: no-cache"')
        parser.add_argument('--exclude-rules', dest='exclude_rules', type=cls.__list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_EXCLUDE_RULES'],
                            help='comma separated list of ZAP addon IDs to exclude from the scan')
        parser.add_argument('--full-scan',
                            dest='full_scan',
                            type=cls.__truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_ENABLED'],
                            help='Run a ZAP Full Scan: https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan')
        parser.add_argument('--auto-update-addons',
                            dest='auto_update_addons',
                            type=cls.__truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTO_UPDATE_ADDONS'],
                            help='Auto-update ZAP addons before running a scan')
        parser.add_argument('--validate-domain',
                            dest='full_scan_domain_validation_required',
                            type=cls.__truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED'],
                            help='Checks the domain has the required headers for running a full-scan:' +
                                 'https://docs.gitlab.com/ee/user/application_security/dast/index.html' +
                                 '#domain-validation')
        parser.add_argument('--availability-timeout',
                            dest='availability_timeout',
                            type=int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_TARGET_AVAILABILITY_TIMEOUT'],
                            help='Time limit in seconds to wait for target availability')

        # ZAP configuration options
        parser.add_argument('-O', dest='zap_api_host_override',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_API_HOST_OVERRIDE'],
                            help='zap: Overrides the hostname defined in the API specification')

        parser.add_argument('-c',
                            '--zap-config-file',
                            dest='zap_config_file',
                            help='ZAP config file to use to INFO, IGNORE or FAIL warnings')
        parser.add_argument('-u',
                            '--zap-config-url',
                            dest='zap_config_url',
                            type=cls.__url,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_CONFIG_URL'],
                            help='URL of the ZAP config file to use to INFO, IGNORE or FAIL warnings')
        parser.add_argument('-g', dest='zap_gen_file',
                            help='zap: generate default config file (all rules set to WARN)')
        parser.add_argument('-m',
                            '--spider-mins',
                            default=1,
                            dest='spider_mins',
                            type=int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SPIDER_MINS'],
                            help='the number of minutes to spider for')
        parser.add_argument('-r', dest='zap_report_html',
                            help='zap: file to write the full ZAP HTML report')
        parser.add_argument('-w', dest='zap_report_md',
                            help='zap: file to write the full ZAP Wiki (Markdown) report')
        parser.add_argument('-x', dest='zap_report_xml',
                            help='zap: file to write the full ZAP XML report')
        parser.add_argument('-a',
                            '--include-alpha-vulnerabilities',
                            dest='zap_include_alpha',
                            type=cls.__truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[
                                'DAST_INCLUDE_ALPHA_VULNERABILITIES'
                            ],
                            help='include the alpha passive and active vulnerability definitions')
        parser.add_argument('-d', dest='zap_debug', action='store_true',
                            help='zap: show debug messages')
        parser.add_argument('-P', dest='zap_port',
                            help='zap: specify listen port')
        parser.add_argument('-D', dest='zap_delay_in_seconds',
                            help='zap: delay in seconds to wait for passive scanning')
        parser.add_argument('-i', dest='zap_default_info', action='store_true',
                            help='zap: default rules not in the config file to INFO')
        parser.add_argument('-I', dest='zap_no_fail_on_warn', action='store_true',
                            help='zap: do not return failure on warning')
        parser.add_argument('-j',
                            '--use-ajax-spider',
                            dest='zap_use_ajax_spider',
                            type=cls.__truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_USE_AJAX_SPIDER'],
                            help='use the AJAX spider in addition to the '
                                 'traditional spider, useful for crawling '
                                 'sites that require JavaScript')
        parser.add_argument('-l', dest='zap_min_level',
                            help='zap: minimum level to show: PASS, IGNORE, INFO, WARN or FAIL, use '
                                 + 'with -s to hide example URLs')
        parser.add_argument('-n', dest='zap_context_file',
                            help='zap: context file which will be loaded prior to spidering the target')
        parser.add_argument('-p', dest='zap_progress_file',
                            help='zap: progress file which specifies issues that are being addressed')
        parser.add_argument('-s', dest='zap_short_format', action='store_true',
                            help='zap: short output format - dont show PASSes or example URLs')
        parser.add_argument('-T', dest='zap_mins_to_wait',
                            help='zap: max time in minutes to wait for ZAP to start and the passive scan to run')
        parser.add_argument('-z', dest='zap_other_options',
                            help='zap: ZAP command line options e.g. -z"-config aaa=bbb -config ccc=ddd"')

        values = parser.parse_args(argv)

        if values.auto_update_addons is None:
            values.auto_update_addons = False

        values.silent = cls.__is_silent(values)

        values.is_api_scan = False if values.api_specification is None else True

        if values.exclude_rules is None:
            values.exclude_rules = []

        if values.request_headers is None:
            values.request_headers = {}

        return values

    @classmethod
    def __truthy(cls, value):
        return value == "true" or value == "True" or value == "1"

    @classmethod
    def __url(cls, value):
        try:
            result = urlparse(value)
            if all([result.scheme, result.netloc]) is not True:
                raise ArgumentTypeError(f"{value} is not a valid URL")
            else:
                return value
        except Exception:
            raise ArgumentTypeError(f"{value} is not a valid URL")

    @classmethod
    def __list_of_urls(cls, values):
        urls = [url.strip() for url in values.split(',')]
        return list(map(cls.__url, urls))

    @classmethod
    def __list_of_strings(cls, values):
        return list([value.strip() for value in values.split(',')])

    @classmethod
    def __dict_of_key_values(cls, values):
        headers = [cls.__dict_from_string(name_value.strip()) for name_value in values.split(',')]
        return {name: value for header in headers for name, value in header.items()}

    @classmethod
    def __dict_from_string(cls, value):
        parts = value.split(':')

        if len(parts) != 2:
            raise ArgumentTypeError('Failed to parse request headers, aborting')

        return {parts[0].strip(): parts[1].strip()}

    @classmethod
    def __is_silent(cls, values):
        if '-silent' in (values.zap_other_options or '') or values.auto_update_addons is False:
            return True

        return False
