#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null
  docker run --name nginx -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro --network test -d nginx:1.17.6 >/dev/null
  docker run --name api-server -v "${PWD}/fixtures/rest-api/nginx.conf":/etc/nginx/conf.d/default.conf --network test -d nginx:1.17.6-alpine >/dev/null

  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force api-server  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_legacy_zap_baseline_entrypoint() {
  docker run --rm -v "${PWD}":/zap/wrk --network test "${BUILT_IMAGE}" /zap/zap-baseline.py -d -t http://nginx \
    >output/test_legacy_zap_baseline_entrypoint.log 2>&1

  # ZAP fails with an exit code depending on the vulnerabilities found.
  # This is commented out until we return a consistent error code https://gitlab.com/gitlab-org/security-products/dast/merge_requests/69
  # assert_equals "2" "$?" "Legacy zap-baseline.py entrypoint was not successful"

  jq . < gl-dast-report.json > output/report_test_legacy_zap_baseline_entrypoint.json

  diff -u <(./normalize_dast_report.py expect/test_legacy_zap_baseline_entrypoint.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_legacy_zap_baseline_entrypoint.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_legacy_zap_full_scan_entrypoint() {
  docker run --rm -v "${PWD}":/zap/wrk --network test "${BUILT_IMAGE}" /zap/zap-full-scan.py -d -t http://nginx \
    >output/test_legacy_zap_full_scan_entrypoint.log 2>&1

  jq . < gl-dast-report.json > output/report_test_legacy_zap_full_scan_entrypoint.json

  # ZAP fails with an exit code depending on the vulnerabilities found.
  # This is commented out until we return a consistent error code https://gitlab.com/gitlab-org/security-products/dast/merge_requests/69
  # assert_equals "0" "$?" "Legacy zap-full-scan.py entrypoint was not successful"

  diff -u <(./normalize_dast_report.py expect/test_legacy_zap_full_scan_entrypoint.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_legacy_zap_full_scan_entrypoint.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_legacy_api_scan_entrypoint() {
  docker run --rm \
    -v "${PWD}/fixtures/rest-api":/zap/wrk \
    --network test \
    --env DAST_EXCLUDE_RULES=100000,100001 \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secrety.secret' \
    "${BUILT_IMAGE}" \
    /zap/zap-api-scan.py -d --api-specification open-api-v2.yml \
    >output/test_legacy_api_scan_entrypoint.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/rest-api/gl-dast-report.json" > output/report_test_legacy_api_scan_entrypoint.json

  diff -u <(./normalize_dast_report.py expect/test_legacy_api_scan_entrypoint.json) \
          <(./normalize_dast_report.py output/report_test_legacy_api_scan_entrypoint.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_legacy_api_scan_entrypoint.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
