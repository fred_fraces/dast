#!/usr/bin/env python

import requests
import os
import sys
import json
import jsonschema


class SecureReportFormatSchema:
    def __init__(self, schema):
        self.schema = schema

    def validate(self, json_file_to_verify):
        # Read in contents of file to verify
        if not os.path.exists(json_file_to_verify):
            print(f'Cannot find {json_file_to_verify}, aborting.')
            sys.exit(1)

        to_verify = json.loads(open(json_file_to_verify).read())
        jsonschema.validate(instance=to_verify, schema=self.schema)


class SecureSchemaGateway:
    def __init__(self):
        self.schema_directory = f'{os.path.dirname(os.path.abspath(__file__))}/downloaded'
        self.project_url = 'https://gitlab.com/gitlab-org/security-products/security-report-schemas'

    def download(self, schema_version):
        os.makedirs(self.schema_directory, exist_ok=True)

        local_schema = f'{self.schema_directory}/dast-schema-{schema_version}.json'
        schema_url = f'{self.project_url}/-/raw/{schema_version}/dist/dast-report-format.json'

        if not os.path.exists(local_schema):
            response = requests.get(schema_url)
            open(local_schema, 'wb').write(response.content)

        return SecureReportFormatSchema(json.loads(open(local_schema).read()))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f'usage: {sys.argv[0]} file-to-verify')
        sys.exit(1)

    schema = SecureSchemaGateway().download('v2.3.0-rc1')
    schema.validate(sys.argv[1])
