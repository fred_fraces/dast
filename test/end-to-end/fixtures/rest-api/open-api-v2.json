{
  "swagger": "2.0",
  "info": {
    "title": "Trees API",
    "description": "API to return some lovely trees",
    "version": "1.0.0"
  },
  "host": "hostname",
  "schemes": ["http"],
  "paths": {
    "/": {
      "get": {
        "summary": "Entrypoint",
        "description": "You should depend on a version of the API directly. Latest version at time of writing is `v1`",
        "produces": ["application/json"],
        "responses": {
          "302": { "description": "Redirect to the latest version of the API" }
        }
      }
    },
    "/v1": {
      "get": {
        "summary": "Version entrypoint",
        "description": "You should *not* call this directly",
        "produces": ["application/json"],
        "responses": {
          "302": { "description": "Redirect to the known trees" }
        }
      }
    },
    "/v1/trees": {
      "get": {
        "summary": "Trees",
        "description": "Lists known trees",
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "List of known trees",
            "schema": {
              "type": "object",
              "required": ["tree"],
              "properties": {
                "tree": {
                  "type": "array",
                  "items": { "$ref": "#/definitions/TreeRef" }
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Creates a new tree",
        "consumes": ["application/json"],
        "parameters": [
          {
            "in": "body",
            "name": "name",
            "description": "The name of the tree",
            "schema": { "$ref": "#/definitions/Tree" }
          }
        ],
        "responses": {
          "201": { "description": "Tree is created" }
        }
      }
    },
    "/v1/tree/{treeId}": {
      "get": {
        "summary": "Returns a tree",
        "parameters": [
          {
            "in": "path",
            "name": "treeId",
            "required": true,
            "type": "integer",
            "minimum": 1,
            "description": "Unique ID of the tree"
          }
        ],
        "responses": {
          "200": { "description": "The details of the requested tree" },
          "404": { "description": "Tree not found" }
        }
      },
      "delete": {
        "summary": "Deletes a tree",
        "parameters": [
          {
            "in": "path",
            "name": "treeId",
            "required": true,
            "type": "integer",
            "minimum": 1,
            "description": "Unique ID of the tree"
          }
        ],
        "responses": {
          "204": { "description": "The requested tree has been deleted" }
        }
      }
    }
  },
  "definitions": {
    "Tree": {
      "type": "object",
      "required": ["tree"],
      "properties": {
        "tree": {
          "type": "object",
          "required": ["name"],
          "properties": {
            "name": { "type": "string" }
          }
        }
      }
    },
    "TreeRef": {
      "type": "object",
      "required": ["tree"],
      "properties": {
        "tree": {
          "type": "object",
          "required": ["ref"],
          "properties": {
            "ref": { "type": "string" }
          }
        }
      }
    }
  }
}
