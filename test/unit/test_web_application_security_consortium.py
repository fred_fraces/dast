from unittest import TestCase
from web_application_security_consortium import WebApplicationSecurityConsortium


class TestWebApplicationSecurityConsortium(TestCase):

    def test_should_return_wasc_reference(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id('16')
        self.assertIn('Directory-Indexing', url)

    def test_should_return_wasc_reference_for_single_digit(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id('1')
        self.assertIn('Insufficient-Authentication', url)

    def test_should_return_wasc_reference_for_single_digit_with_trailing_zero(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id('01')
        self.assertIn('Insufficient-Authentication', url)

    def test_should_return_overview_page_when_not_found(self):
        url = WebApplicationSecurityConsortium().reference_url_for_id('666')
        self.assertEqual(WebApplicationSecurityConsortium.OVERVIEW_REFERENCE, url)

    def test_should_throw_up_when_id_is_not_present(self):
        try:
            WebApplicationSecurityConsortium().reference_url_for_id('')
            self.assertTrue(False)
        except ValueError as e:
            self.assertIn('unable to determine reference url', str(e))

    def test_should_throw_up_when_id_is_not_a_number(self):
        try:
            WebApplicationSecurityConsortium().reference_url_for_id('sixsixsix')
            self.assertTrue(False)
        except ValueError as e:
            self.assertIn('unable to determine reference url', str(e))
