import unittest
from scanned_resources import ScannedResources


class ScannedResourcesTest(unittest.TestCase):

    def setUp(self):
        self.messages = [
            {'url': 'http://nginx/', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': 'GET'},
            {'url': 'http://nginx/myform', 'method': 'POST'}
        ]

        self.test_messages_with_empty_values = [
            {'url': '', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': ''},
            {'url': 'http://nginx/myform', 'method': 'POST'}
        ]

    def test_zap_returns_entries_without_url_field_a_blank_url_should_be_returned(self):
        resources = ScannedResources(self.test_messages_with_empty_values)
        self.assertEqual(resources.scanned_resources()[0]['url'], '')
        self.assertEqual(resources.scanned_resources()[0]['method'], 'GET')
        self.assertEqual(resources.scanned_resources()[1]['url'], 'http://nginx/robots.txt')
        self.assertEqual(resources.scanned_resources()[1]['method'], '')

    def test_returns_formatted_string_methods_with_urls_for_each_request(self):
        resources = ScannedResources(self.messages)
        expect_urls = ["GET http://nginx/", "GET http://nginx/robots.txt", "POST http://nginx/myform"]
        self.assertEqual(resources.scanned_resources_as_string(), '\n'.join(expect_urls))

    def test_request_urls_are_counted(self):
        resources = ScannedResources(self.messages)
        self.assertEqual(resources.count(), 3)

    def test_request_url_and_command_are_parsed(self):
        resources = ScannedResources(self.messages)
        self.assertEqual(resources.scanned_resources()[0]['url'], 'http://nginx/')
        self.assertEqual(resources.scanned_resources()[0]['method'], 'GET')
