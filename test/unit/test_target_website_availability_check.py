import unittest
from target_website_availability_check import TargetWebsiteAvailabilityCheck
from mock_config import ToConfig


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


class TargetWebsiteAvailabilityCheckTest(unittest.TestCase):

    # is_safe_to_scan
    def test_is_safe_to_scan_when_not_full_scanning_and_site(self):
        response = ToObject(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(full_scan=False, full_scan_domain_validation_required=True)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_unsafe_to_scan_when_full_scanning_and_site_is_not_available(self):
        config = ToConfig(full_scan=True, full_scan_domain_validation_required=False)
        check = TargetWebsiteAvailabilityCheck(False, config, response=None)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('site is unavailable', error_message)

    def test_is_safe_to_scan_when_no_domain_validation_required_and_site_does_not_deny_scan(self):
        response = ToObject(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(full_scan=True, full_scan_domain_validation_required=False)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_unsafe_to_scan_when_no_domain_validation_required_and_site_denys_scan(self):
        response = ToObject(headers={'gitlab-dast-permission': 'deny'})
        config = ToConfig(full_scan=True, full_scan_domain_validation_required=False)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('application has explicitly denied DAST scans', error_message)

    def test_is_safe_to_scan_when_domain_validation_required_and_site_does_allows_scan(self):
        response = ToObject(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(full_scan=True, full_scan_domain_validation_required=True)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_safe_to_scan_when_domain_validation_required_and_site_does_does_not_allow_scan(self):
        response = ToObject(headers={'gitlab-dast-permission': ''})
        config = ToConfig(full_scan=True, full_scan_domain_validation_required=True)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('application has not explicitly allowed DAST', error_message)

    def test_is_safe_to_scan_apis(self):
        response = ToObject(headers={'gitlab-dast-permission': 'deny'})
        config = ToConfig(full_scan=True, is_api_scan=True, full_scan_domain_validation_required=True)
        check = TargetWebsiteAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertIn(error_message, '')
