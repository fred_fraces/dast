import unittest
from src.report import ModifiedZapReportFormatter
import factories


def zap_alerts():
    return [
        factories.zap_api.alert(
            sourceid='3',
            other='',
            method='GET',
            evidence='',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            messageId='11',
            url='http://nginx/b.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            id='2'
        ),
        factories.zap_api.alert(
            sourceid='3',
            other='The X-XSS-Protection HTTP response header allows the web server ...',
            method='POST',
            evidence='<form action="/myform" method="POST">',
            plugin_id='10016',
            cwe_id='933',
            confidence='Medium',
            wasc_id='14',
            description='Web Browser XSS Protection is not enabled, or is disabled...',
            messageId='11',
            url='http://nginx/',
            reference='http://blogs.msdn.com/a\nhttp://blogs.msdn.com/b',
            solution='Ensure that the web browsers XSS filter is enabled...',
            param='X-XSS-Protection',
            attack='http://nginx/attack',
            name='Web Browser XSS Protection Not Enabled',
            risk='Low',
            id='1'
        ),
        factories.zap_api.alert(
            sourceid='3',
            other='',
            method='GET',
            evidence='',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            messageId='11',
            url='http://nginx/a.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            id='2'
        )
    ]


def zap_report():
    return {
        '@generated': 'Tue, 22 Oct 2019 01:01:55',
        '@version': 'D-2019-09-23',
        'site': [
            {
                '@port': '80',
                '@host': 'nginx',
                '@name': 'http://nginx',
                'alerts': [
                    {
                        'count': '1',
                        'riskdesc': 'Low (Medium)',
                        'name': 'Web Browser XSS Protection Not Enabled',
                        'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
                        'otherinfo': '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'Web Browser XSS Protection Not Enabled',
                        'instances': [
                            {
                                'attack': 'http://nginx/attack',
                                'evidence': '<form action="/myform" method="POST">',
                                'uri': 'http://nginx/',
                                'param': 'X-XSS-Protection',
                                'method': 'POST'
                            }
                        ],
                        'pluginid': '10016',
                        'riskcode': '1',
                        'wascid': '14',
                        'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
                        'cweid': '933',
                        'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
                    },
                    {
                        'count': '2',
                        'riskdesc': 'Medium (Medium)',
                        'name': 'X-Frame-Options Header Not Set',
                        'reference': '<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'X-Frame-Options Header Not Set',
                        'instances': [
                            {
                                'uri': 'http://nginx/b.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            },
                            {
                                'uri': 'http://nginx/a.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            }
                        ],
                        'pluginid': '10020',
                        'riskcode': '2',
                        'wascid': '15',
                        'solution': '<p>Most modern Web browsers support the X-Frame-Options HTTP header...</p>',
                        'cweid': '16',
                        'desc': '<p>X-Frame-Options header is not included in the HTTP response...</p>'
                    }
                ],
                '@ssl': 'false'
            }
        ]
    }


def spider_scan():
    return {
        'progress': '100',
        'state': 'FINISHED',
        'id': '0'
    }


def spider_scan_result():
    return [
        {
            'urlsInScope': [
                {
                    'url': 'http://f.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '200',
                    'messageId': '1'
                },
                {
                    'url': 'http://e.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '201',
                    'messageId': '2'
                }
            ]
        },
        {
            'urlsOutOfScope': ['http://d.url', 'http://c.url']
        },
        {
            'urlsIoError': [
                {
                    'url': 'http://b.url',
                    'statusReason': 'Internal Server Error',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '500'
                },
                {
                    'url': 'http://a.url',
                    'statusReason': 'Bad Gateway',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '502'
                }
            ]
        }
    ]


class ModifiedZapReportFormatterTest(unittest.TestCase):

    def setUp(self):
        report = zap_report()
        alerts = zap_alerts()
        self.report = ModifiedZapReportFormatter().format(report, alerts, '', spider_scan(), spider_scan_result())

    def test_should_remove_message_ids(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][0])
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][1])

    def test_should_remove_scan_id(self):
        self.assertFalse('scanId' in self.report['spider'])

    def test_urls_should_be_ordered_by_url_and_method(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsInScope'][0]['url'], 'http://e.url')
        self.assertEqual(self.report['spider']['result']['urlsInScope'][1]['url'], 'http://f.url')

        self.assertEqual(len(self.report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][0], 'http://c.url')
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][1], 'http://d.url')

        self.assertEqual(len(self.report['spider']['result']['urlsIoError']), 2)
        self.assertEqual(self.report['spider']['result']['urlsIoError'][0]['url'], 'http://a.url')
        self.assertEqual(self.report['spider']['result']['urlsIoError'][1]['url'], 'http://b.url')

    def test_url_keys_should_be_ordered_alphabetically(self):
        keys = list(self.report['spider']['result'].keys())
        self.assertEqual(keys[0], 'urlsInScope')
        self.assertEqual(keys[1], 'urlsIoError')
        self.assertEqual(keys[2], 'urlsOutOfScope')

        first_in_scope_url = list(self.report['spider']['result']['urlsInScope'][0].keys())
        self.assertEqual(first_in_scope_url[0], 'method')
        self.assertEqual(first_in_scope_url[1], 'processed')
        self.assertEqual(first_in_scope_url[2], 'reasonNotProcessed')
        self.assertEqual(first_in_scope_url[3], 'statusCode')
        self.assertEqual(first_in_scope_url[4], 'statusReason')
        self.assertEqual(first_in_scope_url[5], 'url')

    def test_should_collect_all_scanned_result(self):
        scan_results = [{'urlsOutOfScope': ['http://url.2']},
                        {'urlsOutOfScope': ['http://url.1']}]

        report = ModifiedZapReportFormatter().format(zap_report(), zap_alerts(), '', spider_scan(), scan_results)

        self.assertEqual(len(report['spider']['result']['urlsInScope']), 0)
        self.assertEqual(len(report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][0], 'http://url.1')
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][1], 'http://url.2')
        self.assertEqual(len(report['spider']['result']['urlsIoError']), 0)

    def test_should_include_spider_progress(self):
        self.assertEqual(self.report['spider']['state'], 'FINISHED')
        self.assertEqual(self.report['spider']['progress'], '100')

    def test_should_include_timestamp_and_version(self):
        self.assertEqual(self.report['@generated'], 'Tue, 22 Oct 2019 01:01:55')
        self.assertEqual(self.report['@version'], 'D-2019-09-23')

    def test_should_include_site_metadata_http(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(self.report['site'][0]['@host'], 'nginx')
        self.assertEqual(self.report['site'][0]['@name'], 'http://nginx')
        self.assertEqual(self.report['site'][0]['@port'], '80')
        self.assertEqual(self.report['site'][0]['@ssl'], 'false')

    def test_should_include_site_metadata_https(self):
        alerts = [factories.zap_api.alert(url='https://my.site/page')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@host'], 'my.site')
        self.assertEqual(report['site'][0]['@name'], 'https://my.site')
        self.assertEqual(report['site'][0]['@port'], '443')
        self.assertEqual(report['site'][0]['@ssl'], 'true')

    def test_should_include_site_metadata_custom_port(self):
        alerts = [factories.zap_api.alert(url='https://my.site:558')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@host'], 'my.site')
        self.assertEqual(report['site'][0]['@name'], 'https://my.site:558')
        self.assertEqual(report['site'][0]['@port'], '558')

    def test_site_alerts_include_all_properties(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(alert['alert'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['confidence'], '2')
        self.assertEqual(alert['count'], '1')
        self.assertEqual(alert['cweid'], '933')
        self.assertEqual(alert['desc'], '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>')
        self.assertEqual(alert['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['otherinfo'],
                         '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>')
        self.assertEqual(alert['pluginid'], '10016')
        self.assertEqual(alert['riskcode'], '1')
        self.assertEqual(alert['riskdesc'], 'Low (Medium)')
        self.assertEqual(alert['reference'], '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>')
        self.assertEqual(alert['solution'], '<p>Ensure that the web browsers XSS filter is enabled...</p>')
        self.assertEqual(alert['sourceid'], '3')
        self.assertEqual(alert['wascid'], '14')

    def test_site_alerts_are_ordered_by_risk(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)
        self.assertEqual(self.report['site'][0]['alerts'][0]['name'], 'X-Frame-Options Header Not Set')
        self.assertEqual(self.report['site'][0]['alerts'][1]['name'], 'Web Browser XSS Protection Not Enabled')

    def test_site_alerts_include_instances(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(len(alert['instances']), 1)
        self.assertEqual(alert['instances'][0]['attack'], 'http://nginx/attack')
        self.assertEqual(alert['instances'][0]['evidence'], '<form action="/myform" method="POST">')
        self.assertEqual(alert['instances'][0]['method'], 'POST')
        self.assertEqual(alert['instances'][0]['param'], 'X-XSS-Protection')
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/')

    def test_site_alerts_instances_are_sorted_by_uri(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][0]
        self.assertEqual(len(alert['instances']), 2)
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/a.location')
        self.assertEqual(alert['instances'][1]['uri'], 'http://nginx/b.location')

    def test_site_alerts_otherinfo_is_empty_when_no_content(self):
        alerts = [factories.zap_api.alert(other='')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['otherinfo'], '')

    def test_site_alerts_otherinfo_converts_new_lines_with_carriage_return_to_new_html(self):
        alerts = [factories.zap_api.alert(other='Hello\r\nWorld')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['otherinfo'], '<p>Hello</p><p>World</p>')

    def test_site_alerts_solution_is_empty_html_when_no_content(self):
        alerts = [factories.zap_api.alert(solution='')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['solution'], '<p></p>')

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        zap_rpt = zap_report()
        zap_rpt['site'][0]['alerts'][0]['pluginid'] = ''

        alert = factories.zap_api.alert(id='10', plugin_id='', name='Web Browser XSS Protection Not Enabled')

        try:
            ModifiedZapReportFormatter().format(zap_rpt, [alert], '', spider_scan(), spider_scan_result())
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Unable to create DAST report as there is no pluginid for alert '10'")

    def test_should_support_multiple_sites(self):
        alerts = [
            factories.zap_api.alert(url='http://host.a/path', name='vulnerability.a'),
            factories.zap_api.alert(url='http://host.b/path', name='vulnerability.b')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)

    def test_different_ports_should_have_different_sites(self):
        alerts = [
            factories.zap_api.alert(url='http://host.a/path', name='vulnerability.a'),
            factories.zap_api.alert(url='http://host.b/path', name='vulnerability.b')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)

    def test_http_sites_with_implicit_port_should_be_grouped(self):
        alerts = [
            factories.zap_api.alert(url='http://host.com:80', name='vulnerability.a'),
            factories.zap_api.alert(url='http://host.com', name='vulnerability.b')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@name'], 'http://host.com')
        self.assertEqual(report['site'][0]['@port'], '80')

    def test_https_sites_with_implicit_port_should_be_grouped(self):
        alerts = [
            factories.zap_api.alert(url='https://host.com:443', name='vulnerability.a'),
            factories.zap_api.alert(url='https://host.com', name='vulnerability.b')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(report['site'][0]['@name'], 'https://host.com')
        self.assertEqual(report['site'][0]['@port'], '443')

    def test_http_sites_with_different_ports_should_not_be_grouped(self):
        alerts = [
            factories.zap_api.alert(url='http://host.com:112', name='vulnerability.a'),
            factories.zap_api.alert(url='http://host.com:556', name='vulnerability.b')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 2)
        self.assertEqual(report['site'][0]['@name'], 'http://host.com:112')
        self.assertEqual(report['site'][0]['@port'], '112')
        self.assertEqual(report['site'][1]['@name'], 'http://host.com:556')
        self.assertEqual(report['site'][1]['@port'], '556')

    def test_should_have_empty_cwe_when_zero_value(self):
        alerts = [factories.zap_api.alert(cwe_id='0')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['cweid'], '')

    def test_should_have_empty_wascid_when_zero_value(self):
        alerts = [factories.zap_api.alert(wasc_id='0')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['wascid'], '')

    def test_should_have_informational_riskcode(self):
        alerts = [factories.zap_api.alert(risk='Informational', confidence='High')]
        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())

        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 1)
        self.assertEqual(report['site'][0]['alerts'][0]['riskcode'], '0')
        self.assertEqual(report['site'][0]['alerts'][0]['confidence'], '3')
        self.assertEqual(report['site'][0]['alerts'][0]['riskdesc'], 'Informational (High)')

    def test_alerts_should_be_grouped_by_plugin_id_and_risk(self):
        alerts = [
            factories.zap_api.alert(plugin_id='10001', risk='Low', url='http://site.com/a'),
            factories.zap_api.alert(plugin_id='10001', risk='Low', url='http://site.com/b'),
            factories.zap_api.alert(plugin_id='10001', risk='Informational', url='http://site.com/c'),
            factories.zap_api.alert(plugin_id='10002', risk='Informational', url='http://site.com/d')
        ]

        report = ModifiedZapReportFormatter().format(zap_report(), alerts, '', spider_scan(), spider_scan_result())
        self.assertEqual(len(report['site']), 1)
        self.assertEqual(len(report['site'][0]['alerts']), 3)

        self.assertEqual(report['site'][0]['alerts'][0]['pluginid'], '10001')
        self.assertEqual(len(report['site'][0]['alerts'][0]['instances']), 2)
        self.assertEqual(report['site'][0]['alerts'][0]['instances'][0]['uri'], 'http://site.com/a')
        self.assertEqual(report['site'][0]['alerts'][0]['instances'][1]['uri'], 'http://site.com/b')

        self.assertEqual(report['site'][0]['alerts'][1]['pluginid'], '10001')
        self.assertEqual(len(report['site'][0]['alerts'][1]['instances']), 1)
        self.assertEqual(report['site'][0]['alerts'][1]['instances'][0]['uri'], 'http://site.com/c')

        self.assertEqual(report['site'][0]['alerts'][2]['pluginid'], '10002')
        self.assertEqual(len(report['site'][0]['alerts'][2]['instances']), 1)
        self.assertEqual(report['site'][0]['alerts'][2]['instances'][0]['uri'], 'http://site.com/d')
