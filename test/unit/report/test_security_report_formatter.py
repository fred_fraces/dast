import unittest
from unittest.mock import MagicMock
from src.report import SecurityReportFormatter
from scanned_resources import ScannedResources
import factories


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


def zap_alerts():
    return [
        factories.zap_api.alert(
            sourceid='3',
            other='The X-XSS-Protection HTTP response header allows the web server ...',
            method='POST',
            evidence='<form action="/myform" method="POST">',
            plugin_id='10016',
            cwe_id='933',
            confidence='Medium',
            wasc_id='14',
            description='Web Browser XSS Protection is not enabled, or is disabled...',
            messageId='11',
            url='http://nginx/',
            reference='http://blogs.msdn.com/a\nhttp://blogs.msdn.com/b',
            solution='Ensure that the web browsers XSS filter is enabled...',
            param='X-XSS-Protection',
            attack='http://nginx/attack',
            name='Web Browser XSS Protection Not Enabled',
            risk='Low',
            id='1'
        ),
        factories.zap_api.alert(
            sourceid='3',
            other='The X-XSS-Protection HTTP response header allows the web server ...',
            method='GET',
            evidence='<form action="/myform" method="POST">',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            messageId='11',
            url='http://nginx/b.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            id='2'
        ),
        factories.zap_api.alert(
            sourceid='3',
            other='The X-XSS-Protection HTTP response header allows the web server ...',
            method='GET',
            evidence='<form action="/myform" method="POST">',
            plugin_id='10020',
            cwe_id='16',
            confidence='Medium',
            wasc_id='15',
            description='X-Frame-Options header is not included in the HTTP response...',
            messageId='11',
            url='http://nginx/a.location',
            reference='http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...',
            solution='Most modern Web browsers support the X-Frame-Options HTTP header...',
            param='X-Frame-Options',
            attack='',
            name='X-Frame-Options Header Not Set',
            risk='Medium',
            id='3'
        )
    ]


def zap_report():
    return factories.zap_report.report(alerts=[
        {
            'count': '1',
            'riskdesc': 'Low (Medium)',
            'name': 'Web Browser XSS Protection Not Enabled',
            'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
            'otherinfo': '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
            'sourceid': '3',
            'confidence': '2',
            'alert': 'Web Browser XSS Protection Not Enabled',
            'instances': [
                {
                    'attack': 'http://nginx/attack',
                    'evidence': '<form action="/myform" method="POST">',
                    'uri': 'http://nginx/',
                    'param': 'X-XSS-Protection',
                    'method': 'POST'
                }
            ],
            'pluginid': '10016',
            'riskcode': '1',
            'wascid': '14',
            'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
            'cweid': '933',
            'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
        },
        {
            'count': '2',
            'riskdesc': 'Medium (Medium)',
            'name': 'X-Frame-Options Header Not Set',
            'reference': '<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...</p>',
            'sourceid': '3',
            'confidence': '2',
            'alert': 'X-Frame-Options Header Not Set',
            'instances': [
                {
                    'uri': 'http://nginx/b.location',
                    'param': 'X-Frame-Options',
                    'method': 'GET'
                },
                {
                    'uri': 'http://nginx/a.location',
                    'param': 'X-Frame-Options',
                    'method': 'GET'
                }
            ],
            'pluginid': '10020',
            'riskcode': '2',
            'wascid': '15',
            'solution': '<p>Most modern Web browsers support the X-Frame-Options HTTP header...</p>',
            'cweid': '16',
            'desc': '<p>X-Frame-Options header is not included in the HTTP response...</p>'
        }
    ])


class SecurityReportFormatterTest(unittest.TestCase):

    def setUp(self):
        self.wasc = ToObject(reference_url_for_id=MagicMock(return_value='http://projects.webappsec.org/reference'))
        self.messages = [
            {'url': 'http://nginx/', 'method': 'GET'},
            {'url': 'http://nginx/a.txt', 'method': 'POST'},
            {'url': 'http://nginx/a.txt', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': 'GET'},
            {'url': 'http://nginx/myform', 'method': 'POST'}
        ]
        self.uuid = MagicMock()
        self.uuid.uuid4 = MagicMock(return_value='1234')
        self.scanned_resources = ScannedResources(self.messages)
        alerts = zap_alerts()
        self.report = SecurityReportFormatter(self.wasc, self.uuid).format(zap_report(),
                                                                           alerts,
                                                                           self.scanned_resources)

    def test_should_contain_a_version(self):
        self.assertTrue(self.report['version'])

    def test_when_zap_does_not_return_a_url_it_should_not_error(self):
        scanned_resources = ScannedResources([{'url': '', 'method': 'GET'}])
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap_report(), zap_alerts(), scanned_resources)

        scanned_resource = report['scan']['scanned_resources'][0]
        self.assertEqual(scanned_resource['url'], '')

    def test_scanned_resources_include_all_properties(self):
        self.assertEqual(len(self.report['scan']['scanned_resources']), 5)

        scanned_resource = self.report['scan']['scanned_resources'][4]
        self.assertEqual(scanned_resource['type'], 'url')
        self.assertEqual(scanned_resource['url'], 'http://nginx/robots.txt')
        self.assertEqual(scanned_resource['method'], 'GET')

    def test_scanned_resources_should_be_sorted_by_type_url_and_method(self):
        resource_0 = self.report['scan']['scanned_resources'][0]
        self.assertEqual(resource_0['url'], 'http://nginx/')
        self.assertEqual(resource_0['method'], 'GET')

        resource_1 = self.report['scan']['scanned_resources'][1]
        self.assertEqual(resource_1['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_1['method'], 'GET')

        resource_2 = self.report['scan']['scanned_resources'][2]
        self.assertEqual(resource_2['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_2['method'], 'POST')

        resource_3 = self.report['scan']['scanned_resources'][3]
        self.assertEqual(resource_3['url'], 'http://nginx/myform')
        self.assertEqual(resource_3['method'], 'POST')

        resource_4 = self.report['scan']['scanned_resources'][4]
        self.assertEqual(resource_4['url'], 'http://nginx/robots.txt')
        self.assertEqual(resource_4['method'], 'GET')

    def test_vulnerabilities_include_all_properties(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        vulnerability = self.report['vulnerabilities'][2]
        self.assertEqual(vulnerability['category'], 'dast')
        self.assertEqual(vulnerability['confidence'], 'Medium')
        self.assertEqual(vulnerability['cve'], '10016')
        self.assertEqual(vulnerability['id'], '1234')
        self.assertEqual(vulnerability['description'], 'Web Browser XSS Protection is not enabled, or is disabled...')
        self.assertEqual(len(vulnerability['identifiers']), 3)
        self.assertEqual(vulnerability['identifiers'][0]['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['identifiers'][0]['type'], 'ZAProxy_PluginId')
        self.assertIn('https://github.com/zaproxy/zaproxy/', vulnerability['identifiers'][0]['url'])
        self.assertEqual(vulnerability['identifiers'][0]['value'], '10016')
        self.assertEqual(vulnerability['identifiers'][1]['name'], 'CWE-933')
        self.assertEqual(vulnerability['identifiers'][1]['type'], 'CWE')
        self.assertEqual(vulnerability['identifiers'][1]['url'], 'https://cwe.mitre.org/data/definitions/933.html')
        self.assertEqual(vulnerability['identifiers'][1]['value'], '933')
        self.assertEqual(vulnerability['identifiers'][2]['name'], 'WASC-14')
        self.assertEqual(vulnerability['identifiers'][2]['type'], 'WASC')
        self.assertEqual(vulnerability['identifiers'][2]['url'], 'http://projects.webappsec.org/reference')
        self.assertEqual(vulnerability['identifiers'][2]['value'], '14')
        self.assertEqual(len(vulnerability['links']), 2)
        self.assertEqual(vulnerability['links'][0]['url'], 'http://blogs.msdn.com/a')
        self.assertEqual(vulnerability['links'][1]['url'], 'http://blogs.msdn.com/b')
        self.assertEqual(vulnerability['location']['param'], 'X-XSS-Protection')
        self.assertEqual(vulnerability['location']['method'], 'POST')
        self.assertEqual(vulnerability['location']['hostname'], 'http://nginx')
        self.assertEqual(vulnerability['location']['path'], '/')
        self.assertEqual(vulnerability['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['scanner']['id'], 'zaproxy')
        self.assertEqual(vulnerability['scanner']['name'], 'ZAProxy')
        self.assertEqual(vulnerability['severity'], 'Low')
        self.assertEqual(vulnerability['solution'], 'Ensure that the web browsers XSS filter is enabled...')

    def test_vulnerabilities_are_ordered_by_name_and_risk(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        self.assertEqual(self.report['vulnerabilities'][0]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/a.location', self.report['vulnerabilities'][0]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][1]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/b.location', self.report['vulnerabilities'][1]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][2]['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertIn('/', self.report['vulnerabilities'][2]['location']['path'])

    def test_vulnerability_severity_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_riskcode(riskcode):
            zap = factories.zap_report.report_with_alert(alert_values={'riskcode': riskcode})

            risk_mapping = {'0': 'Informational', '1': 'Low', '2': 'Medium', '3': 'High'}
            alert = factories.zap_api.alert(risk=risk_mapping.get(riskcode, '666'))

            return SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        report = build_report_with_zap_riskcode('0')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Info')

        report = build_report_with_zap_riskcode('1')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Low')

        report = build_report_with_zap_riskcode('2')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Medium')

        report = build_report_with_zap_riskcode('3')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'High')

        report = build_report_with_zap_riskcode('666')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Unknown')

    def test_vulnerability_confidence_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_confidence(confidence):
            zap = factories.zap_report.report_with_alert(alert_values={'confidence': confidence})
            alert = factories.zap_api.alert(confidence=confidence)

            return SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        report = build_report_with_zap_confidence('False Positive')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Ignore')

        report = build_report_with_zap_confidence('Low')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Low')

        report = build_report_with_zap_confidence('Medium')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Medium')

        report = build_report_with_zap_confidence('High')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'High')

        report = build_report_with_zap_confidence('Confirmed')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Confirmed')

        report = build_report_with_zap_confidence('Unknown')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Unknown')

    def test_vulnerability_location_contains_uri_when_contains_query_string(self):
        uri = 'https://fred:flintstone@domain.com/some/path?search=foo#fragment'
        instance = factories.zap_report.alert_instance(uri=uri)
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})
        alert = factories.zap_api.alert(url=uri)

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://domain.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/some/path?search=foo#fragment')

    def test_vulnerability_location_contains_uri_when_has_no_path(self):
        instance = factories.zap_report.alert_instance(uri='https://website.com')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})
        alert = factories.zap_api.alert(url='https://website.com')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '')

    def test_vulnerability_location_contains_uri_when_ends_in_slash(self):
        instance = factories.zap_report.alert_instance(uri='https://website.com/')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})
        alert = factories.zap_api.alert(url='https://website.com/')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/')

    def test_should_not_have_cwe_identifier_when_not_present(self):
        zap = factories.zap_report.report_with_alert(alert_values={'cwe_id': ''})
        alert = factories.zap_api.alert(cwe_id='')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('CWE' in types)

    def test_should_not_have_cwe_identifier_when_has_value_zero(self):
        zap = factories.zap_report.report_with_alert(alert_values={'cwe_id': '0'})
        alert = factories.zap_api.alert(cwe_id='0')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('CWE' in types)

    def test_should_not_links_when_references_are_empty(self):
        zap = factories.zap_report.report_with_alert(alert_values={'reference': ''})
        alert = factories.zap_api.alert(reference='')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(len(report['vulnerabilities'][0]['links']), 0)

    def test_should_not_have_wasc_identifier_when_not_present(self):
        zap = factories.zap_report.report_with_alert(alert_values={'wascid': ''})
        alert = factories.zap_api.alert(wasc_id='')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('WASC' in types)

    def test_should_not_have_wasc_identifier_when_has_zero_value(self):
        zap = factories.zap_report.report_with_alert(alert_values={'wascid': '0'})
        alert = factories.zap_api.alert(wasc_id='0')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('WASC' in types)

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        zap = factories.zap_report.report_with_alert(alert_values={'pluginid': ''})
        alert = factories.zap_api.alert(plugin_id='', name='Web Browser XSS Protection Not Enabled')

        try:
            SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Unable to create DAST report as there is no "
                                     "pluginId for alert 'Web Browser XSS Protection Not Enabled'")

    def test_should_add_evidence_and_other_info(self):
        instance = factories.zap_report.alert_instance(evidence='  378282246310005  ')
        other_info = '<p>Credit Card Type detected: American Express   </p>'
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': other_info})

        other = 'Credit Card Type detected: American Express   \n'
        alert = factories.zap_api.alert(other=other, evidence='  378282246310005  ')
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        evidence = report['vulnerabilities'][-1]['evidence']
        self.assertEqual(evidence['summary'], '378282246310005; '
                                              'Credit Card Type detected: American Express')

    def test_should_exclude_other_info_from_evidence_when_not_present(self):
        instance = factories.zap_report.alert_instance(evidence='error')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})
        alert = factories.zap_api.alert(other='\n', evidence='error')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)
        self.assertEqual(report['vulnerabilities'][-1]['evidence']['summary'], 'error')

    def test_evidence_should_be_empty_when_has_no_value(self):
        instance = factories.zap_report.alert_instance(evidence='')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': ''})
        alert = factories.zap_api.alert(other='', evidence='')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)
        self.assertEqual(report['vulnerabilities'][0]['evidence']['summary'], '')

    def test_should_remove_new_lines(self):
        instance = factories.zap_report.alert_instance(evidence='')
        zap = factories.zap_report.report_with_alert(alert_values={
            'instances': [instance],
            'otherinfo': '\nother\ninfo',
            'solution': '\nsuggested\nsolution\n',
            'desc': 'naughty\nvulnerability',
        })
        alert = factories.zap_api.alert(
            evidence='',
            other='\nother\ninfo',
            solution='\nsuggested\nsolution\n',
            description='naughty\nvulnerability')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['evidence']['summary'], 'other info')
        self.assertEqual(report['vulnerabilities'][0]['solution'], 'suggested solution')
        self.assertEqual(report['vulnerabilities'][0]['description'], 'naughty vulnerability')

    def test_should_condense_spaces_in_description(self):
        zap = factories.zap_report.report_with_alert(alert_values={'desc': 'naughty\n  \nvulnerability'})
        alert = factories.zap_api.alert(description='naughty\n  \nvulnerability')

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, [alert], self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['description'], 'naughty vulnerability')

    def test_vulnerabilities_are_sorted_by_severity(self):
        zap_alert_a = factories.zap_report.alert(name='alert.a', riskcode='0')
        zap_alert_b = factories.zap_report.alert(name='alert.b', riskcode='1')
        zap_alert_c = factories.zap_report.alert(name='alert.c', riskcode='2')
        zap_alert_d = factories.zap_report.alert(name='alert.d', riskcode='3')
        zap = factories.zap_report.report(alerts=[zap_alert_a, zap_alert_c, zap_alert_b, zap_alert_d])

        alert_a = factories.zap_api.alert(name='alert.a', risk='Informational')
        alert_b = factories.zap_api.alert(name='alert.b', risk='Low')
        alert_c = factories.zap_api.alert(name='alert.c', risk='Medium')
        alert_d = factories.zap_api.alert(name='alert.d', risk='High')
        alerts = [alert_a, alert_c, alert_b, alert_d]
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, alerts, self.scanned_resources)

        self.assertEqual(len(report['vulnerabilities']), 4)
        self.assertEqual(report['vulnerabilities'][0]['message'], 'alert.d')
        self.assertEqual(report['vulnerabilities'][1]['message'], 'alert.c')
        self.assertEqual(report['vulnerabilities'][2]['message'], 'alert.b')
        self.assertEqual(report['vulnerabilities'][3]['message'], 'alert.a')
