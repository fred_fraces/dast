import unittest
from unittest.mock import MagicMock
from zaproxy import ZAProxy, TYPE_SPIDER, TYPE_SPIDER_TASK, TYPE_SPIDER_AJAX, TYPE_ZAP_USER
import factories


class ZAProxyTest(unittest.TestCase):

    def setUp(self):
        self.zap = MagicMock()
        self.target = "http://target"
        self.zaproxy = ZAProxy(MagicMock(), self.target, [])

        # new session must get called to steal the `zap` instance
        self.zaproxy.new_session(self.zap, self.target)

    def test_should_return_scanned_resources(self):
        self.zap.core.messages_har = MagicMock(return_value=factories.messages_har([
            {'url': 'http://nginx/type2', 'method': 'GET', 'type': TYPE_SPIDER},
            {'url': 'http://nginx/type9', 'method': 'GET', 'type': TYPE_SPIDER_TASK},
            {'url': 'http://nginx/not_spider', 'method': 'GET', 'type': '12'},
            {'url': 'http://nginx/also_not_spider', 'method': 'GET', 'type': '50'},
            {'url': 'http://nginx/type10', 'method': 'GET', 'type': TYPE_SPIDER_AJAX},
            {'url': 'http://nginx/type11', 'method': 'GET', 'type': TYPE_ZAP_USER}
        ]))

        scanned_resources = self.zaproxy.scanned_resources()

        self.assertEqual(len(scanned_resources), 4)
        self.assertDictEqual(scanned_resources[0], {'method': 'GET', 'url': 'http://nginx/type10'})
        self.assertDictEqual(scanned_resources[1], {'method': 'GET', 'url': 'http://nginx/type11'})
        self.assertDictEqual(scanned_resources[2], {'method': 'GET', 'url': 'http://nginx/type2'})
        self.assertDictEqual(scanned_resources[3], {'method': 'GET', 'url': 'http://nginx/type9'})

    def test_duplicate_scanned_resources_are_removed(self):
        self.zap.core.messages_har = MagicMock(return_value=factories.messages_har([
            {'url': 'http://nginx/', 'method': 'GET', 'type': TYPE_SPIDER},
            {'url': 'http://nginx/', 'method': 'GET', 'type': TYPE_SPIDER},
        ]))

        self.assertEqual(len(self.zaproxy.scanned_resources()), 1)

    def test_messages_with_invalid_content_is_safely_handled(self):
        self.zap.core.messages_har = MagicMock(return_value=factories.messages_har([
            {'url': None, 'method': 'GET', 'type': TYPE_SPIDER},
            {'url': 'http://nginx/robots.txt', 'method': None, 'type': TYPE_SPIDER},
            {'url': 'http://nginx/myform', 'method': 'POST', 'type': TYPE_SPIDER}
        ]))

        scanned_resources = self.zaproxy.scanned_resources()

        self.assertEqual(len(scanned_resources), 3)
        self.assertDictEqual(scanned_resources[0], {'method': 'GET', 'url': ''})
        self.assertDictEqual(scanned_resources[1], {'method': 'POST', 'url': 'http://nginx/myform'})
        self.assertDictEqual(scanned_resources[2], {'method': '', 'url': 'http://nginx/robots.txt'})

    def test_returns_no_resources_when_zap_json_is_invalid(self):
        self.zap.core.messages_har = MagicMock(return_value='THIS IS NOT JSON PARSABLE')
        self.assertEqual(len(self.zaproxy.scanned_resources()), 0)

    def test_returns_no_resources_when_zap_has_none(self):
        self.zap.core.messages_har = MagicMock(return_value='{}')
        self.assertEqual(len(self.zaproxy.scanned_resources()), 0)

    def test_returns_no_resources_when_zap_has_empty_log(self):
        self.zap.core.messages_har = MagicMock(return_value='{log:{}}')
        self.assertEqual(len(self.zaproxy.scanned_resources()), 0)

    def test_returns_no_resources_when_zap_has_empty_entries(self):
        self.zap.core.messages_har = MagicMock(return_value='{log:{entries:[{}]}}')
        self.assertEqual(len(self.zaproxy.scanned_resources()), 0)

    def test_returns_no_resources_when_zap_has_empty_request(self):
        self.zap.core.messages_har = MagicMock(return_value='{log:{entries:[{request:{}}]}}')
        self.assertEqual(len(self.zaproxy.scanned_resources()), 0)
