from unittest.mock import MagicMock
from unittest import TestCase
from scan_script_wrapper import ScanScriptWrapper
from mock_config import ToConfig


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


class ScanScriptWrapperTest(TestCase):
    def setUp(self):
        self.original_main = ToObject(main=MagicMock())
        self.system = MagicMock()
        self.logger = MagicMock()
        self.config = ToConfig(target="http://website")
        self.site_check = ToObject(is_available=MagicMock(return_value=True),
                                   unavailable_reason=MagicMock(return_value=None),
                                   is_safe_to_scan=MagicMock(return_value=(True, None)))
        self.target_website = ToObject(address=MagicMock(return_value="http://website"),
                                       is_configured=MagicMock(return_value=True),
                                       is_url=MagicMock(return_value=True),
                                       check_site_is_available=MagicMock(return_value=self.site_check))

    def test_should_throw_when_target_not_set(self):
        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=False),
                                  check_site_is_available=MagicMock(return_value=self.site_check))
        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_called_once()
        self.original_main.main.assert_not_called()

    def test_should_exit_when_target_not_safe_to_scan(self):
        site_check = ToObject(is_available=MagicMock(return_value=True),
                              unavailable_reason=MagicMock(return_value=None),
                              is_safe_to_scan=MagicMock(return_value=(False, 'a good reason')))

        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=True),
                                  is_url=MagicMock(return_value=True),
                                  check_site_is_available=MagicMock(return_value=site_check))
        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_called_once()
        self.original_main.main.assert_not_called()

    def test_should_continue_scanning_even_if_target_website_is_not_available(self):
        site_check = ToObject(is_available=MagicMock(return_value=False),
                              unavailable_reason=MagicMock(return_value='a connect error'),
                              is_safe_to_scan=MagicMock(return_value=(True, None)))

        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=True),
                                  is_url=MagicMock(return_value=True),
                                  check_site_is_available=MagicMock(return_value=site_check))

        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_not_called()
        self.original_main.main.assert_called_once()

    def test_should_skip_target_check_when_specification_is_api_file(self):
        config = ToConfig(api_specification='spec.yml', auto_update_addons=True)

        target_website = ToObject(is_configured=MagicMock(return_value=False), is_url=MagicMock(return_value=False))

        ScanScriptWrapper(self.original_main, config, self.logger, target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('spec.yml', call_args)

    def test_should_update_zap_addons_when_enabled(self):
        config = ToConfig(target="http://website", auto_update_addons=True)

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-addonupdate', call_args[-1])

    def test_should_set_format_as_openapi_when_performing_api_scan(self):
        config = ToConfig(api_specification="api.spec")

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-f', call_args)
        self.assertIn('openapi', call_args)

    def test_should_not_be_zap_baseline_api_scan_when_not_an_api_scan(self):
        config = ToConfig(target="http://website")

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertNotIn('-S', call_args)

    def test_should_not_be_zap_baseline_api_scan_when_api_scan_is_full_scan(self):
        config = ToConfig(api_specification="api.spec", full_scan=True)

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertNotIn('-S', call_args)

    def test_should_be_zap_baseline_api_scan_when_api_scan_is_not_full_scan(self):
        config = ToConfig(api_specification="api.spec", full_scan=False)

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-S', call_args)

    def test_should_use_api_specification_as_zap_target(self):
        config = ToConfig(api_specification="http://site/api.spec")

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-t', call_args[0])
        self.assertIn('http://site/api.spec', call_args[1])

    def test_should_not_update_zap_addons_when_disabled(self):
        config = ToConfig(target="http://website", auto_update_addons=False)

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertNotIn('-addonupdate', call_args[-1])

    def test_should_exclude_rules(self):
        config = ToConfig(target="http://website", exclude_rules=['1001'])

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-config globalalertfilter.filters.filter(0).ruleid=1001', call_args[-1])
        self.assertIn('-config globalalertfilter.filters.filter(0).url=.*', call_args[-1])
        self.assertIn('-config globalalertfilter.filters.filter(0).urlregex=true', call_args[-1])
        self.assertIn('-config globalalertfilter.filters.filter(0).newrisk=-1', call_args[-1])
        self.assertIn('-config globalalertfilter.filters.filter(0).enabled=true', call_args[-1])

    def test_does_not_exclude_rules_when_none_given(self):
        config = ToConfig(target="http://website", exclude_rules=[])

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertNotIn('globalalertfilter.filters.filter(0).ruleid', call_args)

    def test_should_add_request_headers(self):
        config = ToConfig(target="http://website", request_headers={'Authorization': 'Bearer my.token'})

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertIn('-config replacer.full_list(0).description=header_0', call_args[-1])
        self.assertIn('-config replacer.full_list(0).enabled=true', call_args[-1])
        self.assertIn('-config replacer.full_list(0).matchtype=REQ_HEADER', call_args[-1])
        self.assertIn('-config replacer.full_list(0).matchstr=Authorization', call_args[-1])
        self.assertIn('-config replacer.full_list(0).regex=false', call_args[-1])
        self.assertIn('-config replacer.full_list(0).replacement="Bearer my.token"', call_args[-1])

    def test_does_not_add_request_headers_when_there_are_none(self):
        config = ToConfig(target="http://website", request_headers={})

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertNotIn('config replacer.full_list(0).description=header_0', call_args)

    def test_should_pass_zap_arguments_to_zap(self):
        config = ToConfig(
            target="http://website",
            zap_config_file='config.file',
            zap_config_url='config.url',
            zap_gen_file='gen.file',
            spider_mins='10',
            zap_report_html='report.html',
            zap_report_md='report.md',
            zap_report_xml='report.xml',
            zap_include_alpha=True,
            zap_debug=True,
            zap_port='6001',
            zap_delay_in_seconds='50',
            zap_default_info=True,
            zap_no_fail_on_warn=True,
            zap_use_ajax_spider=True,
            zap_min_level='INFO',
            zap_context_file='context.file',
            zap_progress_file='progress.file',
            zap_short_format=True,
            zap_mins_to_wait='1',
            zap_api_host_override='my.host.com',
            zap_other_options='a=b c=d')

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        call_args = self.original_main.main.call_args[0][0]
        self.assertEqual(call_args[0], '-t')
        self.assertEqual(call_args[1], 'http://website')
        self.assertEqual(call_args[2], '-J')
        self.assertEqual(call_args[3], 'gl-dast-report.json')
        self.assertEqual(call_args[4], '-c')
        self.assertEqual(call_args[5], 'config.file')
        self.assertEqual(call_args[6], '-u')
        self.assertEqual(call_args[7], 'config.url')
        self.assertEqual(call_args[8], '-g')
        self.assertEqual(call_args[9], 'gen.file')
        self.assertEqual(call_args[10], '-m')
        self.assertEqual(call_args[11], '10')
        self.assertEqual(call_args[12], '-r', 'report.html')
        self.assertEqual(call_args[13], 'report.html')
        self.assertEqual(call_args[14], '-w')
        self.assertEqual(call_args[15], 'report.md')
        self.assertEqual(call_args[16], '-x')
        self.assertEqual(call_args[17], 'report.xml')
        self.assertEqual(call_args[18], '-a')
        self.assertEqual(call_args[19], '-d')
        self.assertEqual(call_args[20], '-P')
        self.assertEqual(call_args[21], '6001')
        self.assertEqual(call_args[22], '-D')
        self.assertEqual(call_args[23], '50')
        self.assertEqual(call_args[24], '-i')
        self.assertEqual(call_args[25], '-I')
        self.assertEqual(call_args[26], '-j')
        self.assertEqual(call_args[27], '-l')
        self.assertEqual(call_args[28], 'INFO')
        self.assertEqual(call_args[29], '-n')
        self.assertEqual(call_args[30], 'context.file')
        self.assertEqual(call_args[31], '-p')
        self.assertEqual(call_args[32], 'progress.file')
        self.assertEqual(call_args[33], '-s')
        self.assertEqual(call_args[34], '-T')
        self.assertEqual(call_args[35], '1')
        self.assertEqual(call_args[36], '-O')
        self.assertEqual(call_args[37], 'my.host.com')
        self.assertEqual(call_args[38], '-z')
        self.assertIn('a=b c=d', call_args[-1])
