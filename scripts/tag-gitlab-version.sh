#!/bin/sh

set -e

printf "\n\n######### Initializing environment variables #########\n"

if [ -z "$DAST_VERSION" ]; then
  printf "Aborting, environment variable DAST_VERSION must contain a version of DAST that you would like to tag with the GitLab runner version e.g. 1.6.1\n"
  exit 1
fi

# required file paths
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"
FILE_CONTAINING_SUPPORTED_GITLAB_VERSION=$(realpath "$WORKING_DIRECTORY/../supported-gitlab-runner.txt")

if ! [ -f "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION" ]; then
  printf "Aborting, unable to determine supported GitLab version as the file containing the version doesn't exist: '%s'.\n" "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION"
  exit 1
fi

GITLAB_RUNNER_VERSION=$(cat "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION")

printf "\n\n######### Authenticating with Docker #########\n"
docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

printf "\n\n######### Retrieving DAST Docker image %s so it can be tagged #########\n" "$DAST_VERSION"
docker pull "$CI_REGISTRY_IMAGE:$DAST_VERSION"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$GITLAB_RUNNER_VERSION"
docker tag "$CI_REGISTRY_IMAGE:$DAST_VERSION" "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"
docker push "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"
