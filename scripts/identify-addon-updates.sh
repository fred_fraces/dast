#!/bin/sh

set -eu

BUILT_IMAGE=${BUILT_IMAGE:-dast}
OUT="$(mktemp).out"

cleanup() {
    docker rm --force identify-updates-nginx >/dev/null 2>&1 || true
    docker network rm identify-updates >/dev/null 2>&1 || true
    rm "$OUT" >/dev/null 2>&1 || true
}

trap cleanup EXIT

docker network create identify-updates >/dev/null

docker run \
       --name identify-updates-nginx \
       -v "${PWD}/test/end-to-end/fixtures/basic-site":/usr/share/nginx/html:ro \
       -v "${PWD}/test/end-to-end/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf \
       -v "${PWD}/test/end-to-end/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
       -v "${PWD}/test/end-to-end/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
       --network identify-updates -d nginx:1.17.6 >/dev/null

docker run --rm --network identify-updates "$BUILT_IMAGE" \
       /analyze -d --auto-update-addons true -t http://identify-updates-nginx \
       > "$OUT" 2>&1

NEEDLE="Installing new addon"
if grep -q "$NEEDLE" < "$OUT"; then
    echo "The following addons are available for update:"
    grep "$NEEDLE" < "$OUT" | sed -n -e "s/.*$NEEDLE //p"
    exit 1
else
    echo "No updates available"
fi
